<?php
class Customer_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    
    public function Log_in()
    {
        $sql = "SELECT * FROM DetailCustomer where cus_Password= ? AND cus_Email = ?";
        
        $hashPassword = $this->input->post('passwordText');
        $query = $this->db->query($sql, array($hashPassword, $this->input->post('emailText')));
        
        return $query->result();
    }
    
    public function Register()
    {
        $NoErrorOccured = True;
        try{
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $email = $this->input->post('emailTextr');
        $password = $this->input->post('passwordTextr');
        $phone = $this->input->post('phone');
        $address = $this->input->post('address');
        $data = array(
        'cus_Firstname' => $fname,
        'cus_LastName' => $lname,
        'cus_Phone' => $phone,
        'cus_Email' => $email,
        'cus_ShippingAddress' => $address,
        'cus_Password' => $password
        );
        
        
            $this->db->insert('DetailCustomer', $data);
            
        }
        catch(Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            $NoErrorOccured = False;
        }
        finally {
            echo "First finally.\n";
        }
        return $NoErrorOccured;
    }
    
    public function Get_Indiv_CustomerInfo($id)
    {
        $sql = "SELECT * FROM DetailCustomer where cus_ID = ?";
        $query = $this->db->query($sql, array($id));
        return $query->row();
        
    }
    public function GetListCustomers()
    {
        $sql = "select * from DetailCustomer";
        $query = $this->db->query($sql);
        return $query->result_array();
        
    }
    public function DeleteCustomer($id)
    {
        $sql = "delete from DetailCustomer where cus_ID = ?";
        $this->db->query($sql, array($id));
    }
    public function best_three_customers()
    {
        $sql = "SELECT top3C.cus_ID, top3C.totalVal, cus.cus_Firstname, cus.cus_LastName, cus.cus_Phone, cus.cus_Email
FROM(SELECT cus_ID, sum(orderTotal) AS totalVal FROM(
SELECT  oDate, subq.orderNo,orderTotal, cus_ID
FROM (SELECT * FROM bmc353_4.OrderDetail as ordD
WHERE DATEDIFF(CURDATE(),oDate) < 365) AS subq, bmc353_4.Orders AS ord
WHERE ord.orderNo = subq.orderNo
GROUP BY ord.orderNo) AS sub2
GROUP BY cus_ID
ORDER BY totalVal DESC
LIMIT 3) as top3C, bmc353_4.DetailCustomer as cus
WHERE top3C.cus_ID = cus.cus_ID
ORDER BY totalVal DESC;
";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}