<?php
class Shipping_model extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }
    
    public function getShippingInfo($orderNo)
    {
        $sql = "SELECT * FROM Shippment where orderNo = ?";
        $query = $this->db->query($sql, $orderNo);
        return $query->row();
    }
    public function update_shipping($shipmentNo)
    {
        $receivedDate = $this->input->post('receivedDate');
        $shipDate = $this->input->post('shipDate');
        $sql = "UPDATE Shippment SET receivedDate=?, shipDate=coalesce(@?,?) WHERE shipmentNo = ?";
        $query = $this->db->query($sql, array($receivedDate, $shipDate, $shipDate, $shipmentNo));
    }
    
}