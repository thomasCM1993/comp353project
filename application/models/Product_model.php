<?php
class Product_model extends CI_Model {
    public function __construct()
    {
        $this->load->database();
        $this->load->helper('date');
    }
    
    public function get_products()
    {
        $sql ="Select Product.pName, Color.Item_Color, Product.pDescription, Inventory.itemNo
FROM Product, Color, Inventory
WHERE Product.pNumber = Inventory.pNumber AND Color.itemNo = Inventory.itemNo AND Color.pNumber = Product.pNumber";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    public function get_specific_product($productID)
    {
        $sql = "Select Product.pName, Color.Item_Color, Product.pDescription, Inventory.itemNo, Inventory.PriceOfUnit, Inventory.NumberOfUnitsProduced
FROM Product, Color, Inventory
WHERE Inventory.itemNo =? AND Product.pNumber = Inventory.pNumber AND Color.itemNo = Inventory.itemNo AND Color.pNumber = Product.pNumber";
        $query = $this->db->query($sql, array($productID));
        return $query->result_array();
    }
    
    public function get_inventory()
    {
        $sql = "select * from Inventory i join Color c on i.itemNo = c.itemNo join Product p on p.pNumber = c.pNumber";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function create_new_product()
    {
        $pName = $this->input->post('pName');
        $pDescription = $this->input->post('pDescription');
        $PriceOfUnit = $this->input->post('PriceOfUnit');
        $NumberOfUnitsProduced = $this->input->post('NumberOfUnitsProduced');
        $Item_Color = $this->input->post('Item_Color');
        
        $dateStart = date('Y-m-d H:i:s',now());
        $data1 = array(
                'pName' => $pName,
                'pDescription' => $pDescription
        );
        
        $this->db->insert('Product', $data1);
        $insert_id = $this->db->insert_id();
        
        
        $data2 = array(
                'pNumber' => $insert_id,
                'ItemDateOfManufacturing' => $dateStart,
                'NumberOfUnitsProduced' => $NumberOfUnitsProduced,
                'PriceOfUnit' => $PriceOfUnit
        );
        $this->db->insert('Inventory', $data2);
        $id2 = $this->db->insert_id();
        $data3 = array(
                'pNumber' => $id,
                'Item_Color' => $Item_Color,
                'itemNo' => $id2
        );
        $this->db->insert('Color', $data3);
    }
    public function incress_inventory($id)
    {
        $sql = "update Inventory set NumberOfUnitsProduced = NumberOfUnitsProduced + 10 where itemNo = ?";
        $query = $this->db->query($sql, array($id));
        
    }
    public function removeProduct($id, $id2)
    {
        $sql = "delete from Inventory where itemNo = ?";
        $sql2 = "delete from Color where ItemNo = ? and pNumber = ?";
        $sql3 = "delete from Product where pNumber = ?";
        
        $this->db->query($sql, array($id));
        $this->db->query($sql2, array($id, $id2));
        $this->db->query($sql3, array($id2));
        
    }
    public function top_three_selling_products()
    {
        $sql = "SELECT sub2.pNumber, sub2.pName, sub2.sumQ, sub2.PriceOfUnit, sub2.costT, SUM(sub2.pNumber) DIV sub2.pNumber AS numOfOrders
FROM bmc353_4.OrderDetail AS ordD,
( SELECt subq.itemNo, inv.pNumber, pName, sumQ, PriceOfUnit, (sumQ*PriceOfUnit) AS costT
	FROM 
		(SELECT  itemNo, SUM(oQuantity) AS sumQ
			FROM bmc353_4.OrderDetail
		WHERE DATEDIFF(CURDATE(),oDate) < 365
		GROUP BY itemNo
		ORDER BY  sumQ DESC) AS subq, 
	bmc353_4.Inventory AS inv, bmc353_4.Product AS prod
    WHERE subq.itemNo = inv.itemNo AND inv.pNumber = prod.pNumber
	GROUP BY pNumber
    ORDER BY  costT DESC
    LIMIT 3) AS sub2, 
bmc353_4.Inventory AS inv2
WHERE sub2.pNumber = inv2.pNumber AND inv2.itemNo = ordD.itemNo
GROUP BY sub2.pNumber
ORDER BY costT DESC;";
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}