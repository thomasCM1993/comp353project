<?php
class Employment_model extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }
    
    public function get_Employees()
    {
        //get list of getEmployees
        $sql = "SELECT * FROM Employees";
        $query = $this->db->query($sql);
        return $query->result_array();
        
    }
    public function add_Employees()
    {
        $e_firstName = $this->input->post('e_firstName');
        $e_lastName = $this->input->post('e_lastName');
        $e_dateOfBirth = $this->input->post('e_dateOfBirth');
        $e_position = $this->input->post('e_position');
        $e_address = $this->input->post('e_address');
        $e_SIN = $this->input->post('e_SIN');
        $data = array(
        'e_firstName' => $e_firstName,
        'e_lastName' => $e_lastName,
        'e_position' => $e_position,
        'e_dateOfBirth' => $e_dateOfBirth,
        'e_address' => $e_address,
        'e_SIN' => $e_SIN
        );
        $this->db->insert('Employees', $data);
        $sql = "SELECT * FROM Employees where e_SIN = ?";
        $query = $this->db->query($sql, array($e_SIN))->row()->employee_ID;
        $data2 = array(
            'employee_ID' => $query
        );
        $this->db->insert('Dependants', $data2);
        
        if($this->input->post('Salary') != null || $this->input->post('Salary') != '')
        {
            $data3 = array(
                'Employee_ID' => $query,
                'Salary' => $this->input->post('Salary')
            );
            $this->db->insert('FullTimeEmployee', $data3);
        }
        else if($this->input->post('HourlyRate') != null || $this->input->post('HourlyRate') != '')
        {
            $data4 = array(
                'Employee_ID' => $query,
                'HourlyRate' => $this->input->post('HourlyRate'),
                'NumberOfHoursPerWeek' => $this->input->post('NumberOfHoursPerWeek')
            );
            $this->db->insert('PartTimeEmployee', $data4);
        }
    }
    public function removeEmployee()
    {
        
    }
    
    public function getDepartmentHistory()
    {
        
    }
    
    public function get_specific_employee($employeeID)
    {
        $sql = "SELECT * FROM Employees where employee_ID = ?";
        $query = $this->db->query($sql, array($employeeID))->row();
        return $query;
    }
    public function update_specific_employee($employeeID)
    {
        $e_firstName = $this->input->post('e_firstName');
        $e_lastName = $this->input->post('e_lastName');
        $e_dateOfBirth = $this->input->post('e_dateOfBirth');
        $e_position = $this->input->post('e_position');
        $e_address = $this->input->post('e_address');
        $e_SIN = $this->input->post('e_SIN');
        $data = array(
        'e_firstName' => $e_firstName,
        'e_lastName' => $e_lastName,
        'e_position' => $e_position,
        'e_dateOfBirth' => $e_dateOfBirth,
        'e_address' => $e_address,
        'e_SIN' => $e_SIN
        );
        $this->db->where('employee_ID', $employeeID);
        $this->db->update('Employees', $data);
    }
    public function get_specific_employeeDependent($employeeID)
    {
        $sql = "SELECT * FROM Dependants where employee_ID = ?";
        $query = $this->db->query($sql, array($employeeID))->row();
        return $query;
    }
    public function update_specific_employeeDependent($employeeID)
    {
        $Dependant_SIN = $this->input->post('Dependant_SIN');
        $D_firstName = $this->input->post('D_firstName');
        $D_lastName = $this->input->post('D_lastName');
        $D_dateOfBirth = $this->input->post('D_dateOfBirth');
        $D_phone1 = $this->input->post('D_phone1');
        $D_phone2 = $this->input->post('D_phone2');
        $data = array(
        'Dependant_SIN' => $Dependant_SIN,
        'D_firstName' => $D_firstName,
        'D_lastName' => $D_lastName,
        'D_dateOfBirth' => $D_dateOfBirth,
        'D_phone1' => $D_phone1,
        'D_phone2' => $D_phone2
        
        );
        $this->db->where('employee_ID', $employeeID);
        $this->db->update('Dependants', $data);
    }
    public function get_specific_employeePartTime($employeeID)
    {
        $sql = "SELECT * FROM PartTimeEmployee where Employee_ID = ?";
        $query = $this->db->query($sql, array($employeeID))->row();
        return $query;
    }
    public function get_specific_employeeFullTime($employeeID)
    {
        $sql = "SELECT * FROM FullTimeEmployee where Employee_ID = ?";
        $query = $this->db->query($sql, array($employeeID))->row();
        return $query;
    }
    
       public function update_specific_employeeAvailability($employeeID)
    {
        
            $this->db->where('EmployeeID', $employeeID);
            $this->db->delete('PartTimeEmployee');
            $this->db->where('EmployeeID', $employeeID);
            $this->db->delete('FullTimeEmployee');
        if($this->input->post('HourlyRate') != '' && $this->input->post('HourlyRate') != null && $this->input->post('NumberOfHoursPerWeek') != '' && $this->input->post('NumberOfHoursPerWeek') != null)
        {
            $hrate = $this->input->post('HourlyRate');
            $hpweek = $this->input->post('NumberOfHoursPerWeek');
            
            $this->db->where('EmployeeID', $employeeID);
            $data = array(
                'employee_ID' => $employeeID,
                'HourlyRate' => $hrate,
                'NumberOfHoursPerWeek' => $data
            );
            $this->db->insert('PartTimeEmployee', $data); 
        }
        else
        {
            $sal = $this->input->post('Salary');
            $data2 = array(
                'employee_ID' => $employeeID,
                'Salary' => $sal
            );
            $this->db->insert('FullTimeEmployee', $data2);
        }
    }
    
    
    public function remove_employee($id)
    {
        $sqlFullTime = "delete from FullTimeEmployee where employee_ID = ?";
        $sqlPartTime = "delete from PartTimeEmployee where employee_ID = ?";
        $sqlDependant = "delete from Dependants where employee_ID = ?";
        $sqlEmployees = "delete from Employees where employee_ID = ?";
        
        $this->db->query($sqlFullTime, array($id));
        $this->db->query($sqlPartTime, array($id));
        $this->db->query($sqlDependant, array($id));
        $this->db->query($sqlEmployees, array($id));
        
    }
    
}