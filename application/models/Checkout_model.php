<?php
class Checkout_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('cart');
    }
    
    public function updateInventory($itemNo,$oQuantity){
        $sql ="SELECT NumberOfUnitsProduced from Inventory where itemNo = ?";
        $query = $this->db->query($sql, $itemNo);
        $qtyLeft = $query->row()->NumberOfUnitsProduced - $oQuantity;
        $data =array(
            'NumberOfUnitsProduced' => $qtyLeft
        );
        $this->db->where('itemNo', $itemNo);
        $this->db->update('Inventory', $data);
        
    }
    
    
    public function createShippingInfo($orderNo){
        $today = date('y-m-j');
        $data = array(
            'receivedDate' => $today,
            'orderNo' => $orderNo
        );
        $this->db->insert('Shippment', $data);
    }
    
    public function createFullPayment($paymentNo){
        
        $today = date('y-m-j');
        $data = array(
            'paymentNo' => $paymentNo,
            'payDate' => $today
        );
        
        $this->db->insert('FullPayment', $data);
                
        
    }
    
    public function createInstallment($paymentNo){
        $amount =$this->cart->total()/4;
        $data = array(
            'amount' =>$amount,
            'paymentNo' => $paymentNo
        );
        $this->db->insert('Installments', $data);
                
        $sql ="SELECT MAX(installNo) as installNo from Installments where paymentNo = ?";
        $query = $this->db->query($sql, $paymentNo);
        
        $today = date('y-m-j');
        $installNo = $query->row()->installNo;
        $data2 = array(
            'installNo' =>$installNo,
            'payDate' => $today
        );
        $this->db->insert('InstallmentHistory', $data2);
    }
    
    public function Get_CustomerInfo($cusID){
        $sql = "SELECT * FROM DetailCustomer where cus_ID = ?";
        $query = $this->db->query($sql, $cusID);
        return $query->row();
    }
    
    public function createDetails($orderID, $oQuantity, $itemNo){
            
            $today = date('y-m-j');
 
            $data = array(
            'oDate' => $today,
            'itemNo' => $itemNo,
            'oQuantity' => $oQuantity,
            'orderNo' => $orderID
            );
            
            $this->db->insert('OrderDetail', $data);

    }

    public function getPaymentNo($orderNo){
        $sql="SELECT MAX(paymentNo) as paymentNo 
        FROM Payment
        where orderNo = ?";
        $query = $this->db->query($sql, $orderNo);
        return $query->row();
            
    }
    
    public function createPayment($orderID){
        $compiledAddress = $this->input->post('paymentAddress1').$this->input->post('paymentAddress2').$this->input->post('ccCity').$this->input->post('ccPostal_Code').$this->input->post('ccCountry');
        $ccType = $this->input->post('ccType');
        $ccNum  = $this->input->post('ccNum');
        $CVS = $this->input->post('CVS');
        $dateExpiry = $this->input->post('expiry');
            
        $data = array(
        'ccAddr' => $compiledAddress,
        'ccType' => $ccType,
        'ccNum' => $ccNum,
        'CVS' => $CVS,
        'dateExpiry' => $dateExpiry,
        'orderNo' => $orderID,
        );
        
            $this->db->insert('Payment', $data);
    }
    
    public function createOrder() {
        $total =$this->cart->total();
        $cus_ID = $_SESSION['userID'];
        
        $data = array(
        'orderTotal' => $total,
        'cus_ID' => $cus_ID
        );
            $this->db->insert('Orders', $data);
    }
        
    public function getOrderNo($id){
        $sql="SELECT MAX(orderNo) as orderNo 
        FROM Orders
        where cus_ID =?";
        $query = $this->db->query($sql, $id);
        return $query->row();
            
    }
}