<?php
class Invoice_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function GetListCustomerInfo($id)
    {
        $sql = "SELECT DetailCustomer.cus_Firstname, DetailCustomer.cus_LastName, DetailCustomer.cus_ShippingAddress, DetailCustomer.cus_Phone,
Orders.orderNo, Shippment.receivedDate, Shippment.shipDate, Payment.ccAddr, Payment.ccType, Orders.orderTotal
FROM DetailCustomer, Shippment, Payment, Orders
WHERE Orders.orderNo =? AND Orders.cus_ID = DetailCustomer.cus_ID AND Payment.orderNo = Orders.orderNo AND Shippment.orderNo = Orders.orderNo ";
        $query = $this->db->query($sql, $id);
        return $query->result_array();
        
    } 
    
     public function GetListOrderInfo($id)
    {
        $sql = "SELECT Product.pName, Color.Item_Color, Inventory.PriceOfUnit, OrderDetail.oQuantity
FROM OrderDetail, Product, Inventory, Color
WHERE OrderDetail.orderNo = ? AND OrderDetail.itemNo = Inventory.itemNo AND Inventory.pNumber = Product.pNumber AND Inventory.itemNo = Color.itemNo";
        $query = $this->db->query($sql, $id);
        return $query->result_array();
    } 
    
    public function GetListInstallments($id)
    {
        $sql= "SELECT InstallmentHistory.payDate, Installments.amount
FROM InstallmentHistory, Installments, Payment
WHERE Payment.orderNo = ? AND Installments.paymentNo = Payment.paymentNo AND InstallmentHistory.installNo = Installments.installNo ";
        $query = $this->db->query($sql, $id);
        return $query->result_array();
         
    }
}