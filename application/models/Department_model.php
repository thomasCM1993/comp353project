<?php
class Department_model extends CI_Model {
    public function __construct()
    {
        $this->load->database();
        $this->load->helper('date');
    }
    public function get_departments()
    {
        $sql = "select * from Departments d join DepartmentManager dm on d.Department_ID = dm.Department_ID join Employees e on dm.employee_ID = e.employee_ID";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function get_department_historyFT($departmentID)
    {
        /*$sql = "select Dependants.employee_ID, count(Dependants.employee_ID) as Dependents, Employees.e_firstName as FirstName, Employees.e_lastName as LastName, FullTimeEmployee.Salary/12.00 as MonthlySalaryFT
from Dependants, Employees, FullTimeEmployee
WHERE Dependants.employee_ID IN (select employee_ID from EmployeeDepartmentHistory where Department_ID =? and enddate is null) 
AND Dependants.employee_ID = Employees.employee_ID AND Employees.employee_ID = FullTimeEmployee.employee_ID
group by Dependants.employee_ID";*/
        $sql = "select *, count(d.employee_ID) as numberofDep from 
EmployeeDepartmentHistory edh
join Employees e on edh.Employee_ID = e.employee_ID 
join FullTimeEmployee pe on e.employee_ID = pe.employee_ID
join Dependants d on d.employee_ID = e.employee_ID
where edh.Department_ID = ?
group by d.employee_ID";
        $query = $this->db->query($sql, array($departmentID));
        return $query->result_array();
    }
    
    public function get_department_historyPT($departmentID)
    {
        /*$sql = "
select Dependants.employee_ID, count(Dependants.employee_ID) as Dependents, Employees.e_firstName as FirstName, Employees.e_lastName as LastName, PartTimeEmployee.HourlyRate*PartTimeEmployee.NumberOfHoursPerWeek*4 as MonthlySalaryPT
from Dependants, Employees, PartTimeEmployee
WHERE Dependants.employee_ID IN (select employee_ID from EmployeeDepartmentHistory where Department_ID =? and enddate is null) 
AND Dependants.employee_ID = Employees.employee_ID AND Employees.employee_ID = PartTimeEmployee.employee_ID
group by Dependants.employee_ID";*/
        $sql = "select *, count(d.employee_ID) as numberofDep from 
EmployeeDepartmentHistory edh
join Employees e on edh.Employee_ID = e.employee_ID 
join PartTimeEmployee pe on e.employee_ID = pe.employee_ID
join Dependants d on d.employee_ID = e.employee_ID
where edh.Department_ID = ?
group by d.employee_ID";
        $query = $this->db->query($sql, array($departmentID));
        return $query->result_array();
    }
    
    public function get_full_department_history()
    {
        
    }
    public function create_new_department()
    {
        $Department_Name = $this->input->post('Department_Name');
        $Department_RoomNumber = $this->input->post('Department_RoomNumber');
        $Department_FaxNumber = $this->input->post('Department_FaxNumber');
        $Department_phone1 = $this->input->post('Department_phone1');
        $Department_phone2 = $this->input->post('Department_phone2');
        $data = array(
        'Department_Name' => $Department_Name,
        'Department_RoomNumber' => $Department_RoomNumber,
        'Department_FaxNumber' => $Department_FaxNumber,
        'Department_phone1' => $Department_phone1,
        'Department_phone2' => $Department_phone2,
        );
        $this->db->insert('Departments', $data);
        
        $sql = "select * from Departments where Department_Name = ? and Department_RoomNumber = ?";
        $query = $this->db->query($sql, array($Department_Name, $Department_RoomNumber))->row();
        if($query != "")
        {
            $Department_ID = $query->Department_ID;
            $employee_ID = $this->input->post('Employees');
            $dateStart = date('Y-m-d H:i:s',now());
            $data2 = array(
                'Department_ID' => $Department_ID,
                'employee_ID' => $employee_ID,
                'startDate' => $dateStart
            );
             $this->db->insert('DepartmentManager', $data2);
            $data3 = array(
                'Department_ID' => $Department_ID,
                'Employee_ID' => $employee_ID,
                'startDate' => $dateStart
            );
            $this->db->insert('EmployeeDepartmentHistory', $data3);
                
        }
    }
    public function Delete_Department($id)
    {
        $sql2 = "delete from DepartmentManager where Department_ID = ?";
        $sql1 = "delete from Departments where Department_ID = ?";
        $sql3 = "delete from EmployeeDepartmentHistory where Department_ID = ?";
        
        $this->db->query($sql2, array($id));
        $this->db->query($sql1, array($id));
        $this->db->query($sql3, array($id));
    }
    public function Add_toDepartment($id)
    {
        $dateNow = date('Y-m-d H:i:s',now());
        $employee_ID = $this->input->post('Employees');
        
        
        $sql = "update EmployeeDepartmentHistory
            set EndDate = ?
            where employee_ID = ?";
        $this->db->query($sql, array($dateNow, $employee_ID));
        
        
        
        $sql2 = "insert into EmployeeDepartmentHistory (Department_ID, Employee_ID, startDate) values (?, ?, ?)";
        $this->db->query($sql2, array($id, $employee_ID, $dateNow));
        
    }
}