<?php
class Order_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }
    
   
    public function GetListOrders($id)
    {
        $sql = "select DISTINCT Orders.orderNo as OrderNumber, (OrderDetail.oDate) as OrderDate, Orders.orderTotal
FROM Orders, OrderDetail
where Orders.cus_ID= ? AND OrderDetail.orderNo=Orders.orderNo";
        $query = $this->db->query($sql, $id);
        return $query->result_array();
        
    }
    
    public function get_outstanding_payments()
    {
        $sql = "select * , (o.orderTotal - sum(amount)) as outstanding  from 
Orders o join Payment p on o.orderNo = p.orderNo 
join Installments i on p.paymentNo = i.paymentNo 
join InstallmentHistory ih on i.installNo = ih.installNo
join DetailCustomer dc on dc.cus_ID = o.cus_ID
group by o.orderNo";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function get_average_price()
    {
        $Month = $this->input->post('datepicker');
        $m = substr($Month, 0, 2);
        $day = substr($Month, 3,2);
        $year = substr($Month, 6,9);
        $time = $year.'-'.$m.'-'.$day;
        
        $sql = "SELECT OrderYear, OrderMonth, SUM(orderTotal) AS totalPrice, SUM(sumq) AS itemCount, (SUM(orderTotal)/SUM(sub.sumq) ) AS averageItemPrice
FROM
(SELECT EXTRACT(YEAR FROM oDate) AS OrderYear, EXTRACT(MONTH FROM oDate) AS OrderMonth, orderTotal, SUM(oQuantity) AS sumq
FROM bmc353_4.OrderDetail AS ordD,bmc353_4.Orders AS ord
WHERE ordD.orderNo = ord.orderNo AND EXTRACT(YEAR FROM oDate) = ? AND EXTRACT(MONTH FROM oDate) = ?
GROUP BY ordD.orderNo) AS sub;";
        
        $query = $this->db->query($sql, array($year, $m));
        return $query->result_array();
    }
    
    public function removeOrder($orderNo){
        $this->db->delete('Orders', array('orderNo' => $orderNo)); 
    }
}