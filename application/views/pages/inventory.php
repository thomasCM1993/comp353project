<script>
    $(document).ready(function () {
        $("#AddDepartmentForm").hide();
        $("#btnAddDepartment").click(function () {

            $("#mainDepartmentDiv").hide("slide", {
                direction: "left"
            }, 500);
            $("#AddDepartmentForm").show("slide", {
                direction: "right"
            }, 800);

        });

        $("#CancelAdd").click(function () {
            $("#AddDepartmentForm").hide("slide", {
                direction: "right"
            }, 500);
            $("#mainDepartmentDiv").show("slide", {
                direction: "left"
            }, 800);

        });
        $("#SearchBar").keyup(function () {
            _this = this;
            // Show only matching TR, hide rest of them
            $.each($("#tableDepartment tbody tr"), function () {
                if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                    $(this).hide();
                else
                    $(this).show();
            });
        });

    });
</script>

<center>
    <div class="mdl-textfield mdl-js-textfield" style="margin-top: 10px;">
        <input class="mdl-textfield__input" type="text" id="SearchBar">
        <label class="mdl-textfield__label" for="SearchBar">Search...</label>
    </div>

</center>
<div id="mainDepartmentDiv">
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" id="tableDepartment" style="margin: 25px auto; width:100%">
        <thead>
            <tr>
                <th class="mdl-data-table__cell--non-numeric">Product name</th>
                <th class="mdl-data-table__cell--non-numeric">Product description</th>
                <th class="mdl-data-table__cell--non-numeric">Color</th>
                <th class="mdl-data-table__cell--non-numeric">Manufacturing date</th>
                <th>Number of units produced</th>
                <th class="mdl-data-table__cell--non-numeric">Unit price</th>
                <th class="mdl-data-table__cell--non-numeric">Add to inventory(10)</th>
                <th class="mdl-data-table__cell--non-numeric">Remove product</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list_inventory as $item):?>
                <tr>
                    <td class="mdl-data-table__cell--non-numeric" style="word-wrap: break-word">
                        <?php echo $item['pName'];?>
                    </td>
                    
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['pDescription'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect" style="background-color:<?php echo $item['Item_Color'];?>" disabled>
                        </button>
                        
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['ItemDateOfManufacturing'];?>
                    </td>
                    <td>
                        <?php echo $item['NumberOfUnitsProduced'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['PriceOfUnit'];?> <br/>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <a class="mdl-button mdl-js-button mdl-button--fab" href="<?php echo site_url("Inventory/incress_inventory/".$item['itemNo']); ?>">
                            <i class="material-icons">add</i>
                        </a>

                    </td>
                    <td>
                        <a class="mdl-button mdl-js-button mdl-button--fab" id="btnRemoveEmployee" href="<?php echo site_url("Inventory/remove_product/".$item['itemNo']."/".$item['pNumber']); ?>" >
                        <i class="material-icons">remove</i>
                        </a>

                    </td>
                </tr>
                <?php endforeach;?>
        </tbody>
    </table>
    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored" id="btnAddDepartment" style="top: 220px;
position: absolute;
left: 85%;">
        <i class="material-icons">add</i>
    </button>
</div>

<?php echo validation_errors(); ?>
<?php echo form_open('Inventory/createProduct'); ?>
    <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="AddDepartmentForm" style="width:60%; margin:50px auto;">
        <div class="mdl-card__title">
            <h2 class="mdl-card__title-text">Create new product</h2>
        </div>
        <div class="mdl-card__supporting-text" style="height: 420px">
            
            
    
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="pName" name="pName">
        <label class="mdl-textfield__label" for="pName">Product Name...</label>
    </div>
    
    <div class="mdl-textfield mdl-js-textfield">
        <input class="mdl-textfield__input" type="text" id="pDescription" name="pDescription">
        <label class="mdl-textfield__label" for="pDescription">Description...</label>
    </div>
    
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="PriceOfUnit" name="PriceOfUnit">
        <label class="mdl-textfield__label" for="PriceOfUnit">Unit Cost...</label>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:80%">
        <input class="mdl-textfield__input" type="text" id="NumberOfUnitsProduced" name="NumberOfUnitsProduced">
        <label class="mdl-textfield__label" for="NumberOfUnitsProduced">units produced...</label>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="Item_Color" name="Item_Color">
        <label class="mdl-textfield__label" for="Item_Color">Color...</label>
    </div>
            
        </div>
        <div class="mdl-card__actions mdl-card--border">
            <input type="submit" name="submit" value="Create" style="float:right;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/>
            <button id="CancelAdd" name="cancel" style="float:left;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Cancel</button>
        </div>
    </div>
</form>