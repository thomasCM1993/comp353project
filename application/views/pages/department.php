<script>
    $(document).ready(function () {
        $("#AddDepartmentForm").hide();
        $("#btnAddDepartment").click(function () {

            $("#mainDepartmentDiv").hide("slide", {
                direction: "left"
            }, 500);
            $("#AddDepartmentForm").show("slide", {
                direction: "right"
            }, 800);

        });

        $("#CancelAdd").click(function () {
            $("#AddDepartmentForm").hide("slide", {
                direction: "right"
            }, 500);
            $("#mainDepartmentDiv").show("slide", {
                direction: "left"
            }, 800);

        });
        $("#SearchBar").keyup(function () {
            _this = this;
            // Show only matching TR, hide rest of them
            $.each($("#tableDepartment tbody tr"), function () {
                if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                    $(this).hide();
                else
                    $(this).show();
            });
        });

    });
</script>

<center>
    <div class="mdl-textfield mdl-js-textfield" style="margin-top: 10px;">
        <input class="mdl-textfield__input" type="text" id="SearchBar">
        <label class="mdl-textfield__label" for="SearchBar">Search...</label>
    </div>

</center>
<div id="mainDepartmentDiv">
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" id="tableDepartment" style="margin: 25px auto; width:100%">
        <thead>
            <tr>
                <th class="mdl-data-table__cell--non-numeric">Department ID</th>
                <th class="mdl-data-table__cell--non-numeric">Department Name</th>
                <th class="mdl-data-table__cell--non-numeric">Manager Name</th>
                <th class="mdl-data-table__cell--non-numeric">Department Room Number</th>
                <th class="mdl-data-table__cell--non-numeric">Department Fax Number</th>
                <th class="mdl-data-table__cell--non-numeric">Department phone Numbers</th>
                <th class="mdl-data-table__cell--non-numeric">View History</th>
                <th class="mdl-data-table__cell--non-numeric">Remove</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($department_list as $item):?>
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['Department_ID'];?>
                    </td>
                    
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['Department_Name'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['e_firstName'] . " " . $item['e_lastName'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['Department_RoomNumber'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['Department_FaxNumber'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['Department_phone1'];?> <br/>
                        <?php echo $item['Department_phone2'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" href="<?php echo site_url("DepartmentHistory/index/".$item['Department_ID']); ?>">
                            View History
                        </a>

                    </td>
                    <td>
                        <a class="mdl-button mdl-js-button mdl-button--fab" id="btnRemoveEmployee" href="<?php echo site_url("Department/remove_Department/".$item['Department_ID']); ?>" >
                        <i class="material-icons">remove</i>
                        </a>


                    </td>
                    <td>
                        <a class="mdl-button mdl-js-button mdl-button--fab" id="btnRemoveEmployee" href="<?php echo site_url("Department/remove_Department/".$item['Department_ID']); ?>" >
                        <i class="material-icons">remove</i>
                        </a>

                    </td>
                </tr>
                <?php endforeach;?>
        </tbody>
    </table>
    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored" id="btnAddDepartment" style="top: 220px;
position: absolute;
left: 85%;">
        <i class="material-icons">add</i>
    </button>
</div>
<?php echo validation_errors(); ?>
<?php echo form_open('department/createDepartment'); ?>
    <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="AddDepartmentForm" style="width:60%; margin:50px auto;">
        <div class="mdl-card__title">
            <h2 class="mdl-card__title-text">Register</h2>
        </div>
        <div class="mdl-card__supporting-text" style="height: 420px">
            
            
    
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="Department_Name" name="Department_Name">
        <label class="mdl-textfield__label" for="Department_Name">Department name...</label>
    </div>
    
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="Department_RoomNumber" name="Department_RoomNumber">
        <label class="mdl-textfield__label" for="Department_RoomNumber">Room number...</label>
    </div>
    
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="Department_FaxNumber" name="Department_FaxNumber" pattern="\d{3}[\-]\d{3}[\-]\d{4}">
        <label class="mdl-textfield__label" for="Department_FaxNumber">Fax...</label>
        <span class="mdl-textfield__error">Input is not an phone number!</span>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:80%">
        <input class="mdl-textfield__input" type="text" id="Department_phone1" name="Department_phone1" pattern="\d{3}[\-]\d{3}[\-]\d{4}">
        <label class="mdl-textfield__label" for="Department_phone1">Phone number one...</label>
        <span class="mdl-textfield__error">Input is not an phone number!</span>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="Department_phone2" name="Department_phone2" pattern="\d{3}[\-]\d{3}[\-]\d{4}">
        <label class="mdl-textfield__label" for="Department_phone2">Phone number two(Optional)...</label>
        <span class="mdl-textfield__error">Input is not an phone number!</span>
    </div>
            <br/>
            <span>Manager:</span>
            <select id="Employees" name="Employees">
                <?php foreach ($Employee_list as $item):?>
                <option value="<?php echo $item['employee_ID'];?>"> <?php echo $item['e_firstName'] . " " . $item['e_lastName'];?></option >
                <?php endforeach;?>
            </select>
            
            
        </div>
        <div class="mdl-card__actions mdl-card--border">
            <input type="submit" name="submit" value="Create" style="float:right;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/>
            <button id="CancelAdd" name="cancel" style="float:left;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Cancel</button>
        </div>
    </div>
</form>