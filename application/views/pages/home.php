
<style>
.demo-card-square.mdl-card {
  width: 320px;
  height: 320px;
  float: left;
  margin-right: 2%;
  margin-top: 50px;
}
.demo-card-square > .mdl-card__title {
  color: #fff;
  }
</style>
        
       
        <?php foreach ($product_list as $item):?>
            <div class="demo-card-square mdl-card mdl-shadow--2dp">
                <div class="mdl-card__title mdl-card--expand" style="background: url('url')bottom right 15% no-repeat rgb(255,193,7);background-size: cover;">
                
                    <h2 class="mdl-card__title-text" >
                        <?php echo $item['pName']." ".$item['Item_Color'];?>
                    </h2>
                </div>
                <div class="mdl-card__supporting-text">
                    <?php echo $item['pDescription'];?>
                </div>
                <div class="mdl-card__actions mdl-card--border">
                    <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" href="<?php echo site_url("product/index/".$item['itemNo']); ?>">
                        
                        Buy
                    </a>
                </div>
            </div>
        <?php endforeach;?>
        