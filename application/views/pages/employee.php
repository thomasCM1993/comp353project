<script>
    $(document).ready(function () {
        $("#AddEmployeeDiv").hide();
        $("#btnAddEmployee").click(function () {

            $("#mainEmployeeDiv").hide("slide", {
                direction: "left"
            }, 500);
            $("#AddEmployeeDiv").show("slide", {
                direction: "right"
            }, 800);

        });

        $("#CancelAdd").click(function () {
            $("#AddEmployeeDiv").hide("slide", {
                direction: "right"
            }, 500);
            $("#mainEmployeeDiv").show("slide", {
                direction: "left"
            }, 800);

        });

        $("#SearchBar").keyup(function () {
            _this = this;
            // Show only matching TR, hide rest of them
            $.each($("#tableEmployees tbody tr"), function () {
                if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                    $(this).hide();
                else
                    $(this).show();
            });
        });
        $("#PartTime-1").focus(function () {
            $("#Salary").val("");
            $("#fullTimeDiv").slideUp();
            $("#partTimeDiv").slideDown();

        });
        $("#FullTime-2").focus(function () {
            $("#HourlyRate").val("");
            $("#NumberOfHoursPerWeek").val("");
            $("#partTimeDiv").slideUp();
            $("#fullTimeDiv").slideDown();
        });

    });
</script>

<center>
    <div class="mdl-textfield mdl-js-textfield" style="margin-top: 10px;">
        <input class="mdl-textfield__input" type="text" id="SearchBar">
        <label class="mdl-textfield__label" for="SearchBar">Search...</label>
    </div>

</center>
<div id="mainEmployeeDiv">
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" id="tableEmployees" style="margin: 25px auto; width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th class="mdl-data-table__cell--non-numeric">Name</th>
                <th class="mdl-data-table__cell--non-numeric">Date of birth</th>
                <th class="mdl-data-table__cell--non-numeric">Position</th>
                <th class="mdl-data-table__cell--non-numeric">Address</th>
                <th class="mdl-data-table__cell--non-numeric">View</th>
                <th class="mdl-data-table__cell--non-numeric">Remove</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($employee_list as $item):?>
                <tr>
                    <td>
                        <?php echo $item['employee_ID'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['e_firstName'] . " " . $item['e_lastName'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['e_dateOfBirth'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['e_position'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['e_address'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" href="<?php echo site_url("employeeDetail/index/".$item['employee_ID']); ?>">
                            View Detail
                        </a>
                        
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <a class="mdl-button mdl-js-button mdl-button--fab" id="btnRemoveEmployee" href="<?php echo site_url("employee/remove_employee/".$item['employee_ID']); ?>" >
                        <i class="material-icons">remove</i>
                        </a>
                        
                    </td>
                </tr>
                <?php endforeach;?>
        </tbody>
    </table>
    <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored" id="btnAddEmployee" style="top: 220px;
position: absolute;
left: 85%;">
        <i class="material-icons">add</i>
    </button>
</div>


<?php echo form_open('employee/addEmployee'); ?>
    <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="AddEmployeeDiv" style="margin:50px auto;
  height: 500px;
  width: 60%;">
        <div class="mdl-card__title" style="height: 100px;">
            <h2 class="mdl-card__title-text">Add Employee</h2>
        </div>
        <div class="mdl-card__supporting-text" style="height: 420px">

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="e_firstName" id="e_firstName">
                <label class="mdl-textfield__label" for="e_firstName">First name...</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="e_lastName" id="e_lastName">
                <label class="mdl-textfield__label" for="e_lastName">Last name...</label>
            </div>

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="date" name="e_dateOfBirth" id="e_dateOfBirth">
                <label class="mdl-textfield__label" for="e_dateOfBirth"></label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="e_position" id="e_position">
                <label class="mdl-textfield__label" for="e_position">Position...</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="e_address" id="e_address">
                <label class="mdl-textfield__label" for="e_address">Address...</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="number" name="e_SIN" id="e_SIN">
                <label class="mdl-textfield__label" for="e_SIN">SIN...</label>
            </div>
            <br/>
            
             <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="PartTime-1">
                <input type="radio" id="PartTime-1" class="mdl-radio__button" name="options" value="1" checked>
                <span class="mdl-radio__label">Part Time</span>
            </label>
            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="FullTime-2">
                <input type="radio" id="FullTime-2" class="mdl-radio__button" name="options" value="2">
                <span class="mdl-radio__label">Full Time</span>
            </label>
            <div id="partTimeDiv">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input class="mdl-textfield__input" type="text" name="HourlyRate" id="HourlyRate">
                    <label class="mdl-textfield__label" for="HourlyRate">Hourly Rate</label>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input class="mdl-textfield__input" type="text" name="NumberOfHoursPerWeek" id="NumberOfHoursPerWeek">
                    <label class="mdl-textfield__label" for="NumberOfHoursPerWeek">Number Of Hours Per Week</label>
                </div>

            </div>
            <div id="fullTimeDiv" style="display:none">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input class="mdl-textfield__input" type="text" name="Salary" id="Salary">
                    <label class="mdl-textfield__label" for="Salary">Salary</label>
                </div>

            </div>
            <br/>

        </div>
        <div class="mdl-card__actions mdl-card--border">
            <input type="submit" name="submit" value="Add" style="float:left;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" />
            <span class="mdl-textfield__error"><?php echo $error; ?></span>
            <input type="button" id="CancelAdd" name="cancelAdd" value="Cancel" style="float:right;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" />
        </div>
    </div>
    </form>