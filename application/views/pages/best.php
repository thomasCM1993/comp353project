<script>
    $(document).ready(function () {
        $("#SearchBar1").keyup(function () {
            _this = this;
            // Show only matching TR, hide rest of them
            $.each($("#tablebestCus tbody tr"), function () {
                if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                    $(this).hide();
                else
                    $(this).show();
            });
        });
        $("#SearchBar2").keyup(function () {
            _this = this;
            // Show only matching TR, hide rest of them
            $.each($("#tablebestProd tbody tr"), function () {
                if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                    $(this).hide();
                else
                    $(this).show();
            });
        });
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    });
</script>

<center>
    <div class="mdl-textfield mdl-js-textfield" style="margin-top: 10px;">
        <input class="mdl-textfield__input" type="text" id="SearchBar1">
        <label class="mdl-textfield__label" for="SearchBar">Search...</label>
    </div>

</center>
<div id="mainCusDiv">
    <h2 class="mdl-card__title-text">Best Three Customers</h2>
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" id="tablebestCus" style="margin: 25px auto; width:100%">
        <thead>
            <tr>
                <th class="mdl-data-table__cell--non-numeric">Name</th>
                <th class="mdl-data-table__cell--non-numeric">Phone</th>
                <th class="mdl-data-table__cell--non-numeric">Email</th>
                <th class="mdl-data-table__cell--non-numeric">Total value</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cus_list as $item):?>
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['cus_Firstname'] . ' ' .$item['cus_LastName'];?>
                    </td>
                    
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['cus_Phone'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['cus_Email'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['totalVal'];?>
                    </td>
                </tr>
                <?php endforeach;?>
        </tbody>
    </table>
</div>

<center>
    <div class="mdl-textfield mdl-js-textfield" style="margin-top: 10px;">
        <input class="mdl-textfield__input" type="text" id="SearchBar2">
        <label class="mdl-textfield__label" for="SearchBar">Search...</label>
    </div>

</center>
<div id="mainProdDiv">
    <h2 class="mdl-card__title-text">Best Three Products</h2>
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" id="tablebestProd" style="margin: 25px auto; width:100%">
        <thead>
            <tr>
                <th class="mdl-data-table__cell--non-numeric">Product number</th>
                <th class="mdl-data-table__cell--non-numeric">Product name</th>
                <th class="mdl-data-table__cell--non-numeric">SumQ</th>
                <th class="mdl-data-table__cell--non-numeric">Unit price</th>
                <th class="mdl-data-table__cell--non-numeric">Total Cost</th>
                <th class="mdl-data-table__cell--non-numeric">Number of orders</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($prod_list as $item):?>
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['pNumber'];?>
                    </td>
                    
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['pName'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['sumQ'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['PriceOfUnit'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['costT'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['numOfOrders'];?>
                    </td>
                </tr>
                <?php endforeach;?>
        </tbody>
    </table>
</div>



<?php echo form_open('ThreeBest/loadAverage/'); ?>
    <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="AddEmployeeDiv" style="margin:50px auto;
  height: 300px;
  width: 80%;">
        <div class="mdl-card__title" style="height: 100px;">
            <h2 class="mdl-card__title-text">Average Price of all Items Ordered</h2>
        </div>
        <div class="mdl-card__supporting-text" style="height: 100px">
            <span>Date: </span> <input type="text" id="datepicker" name="datepicker">

        </div>
        
        <div class="mdl-card__actions mdl-card--border">
            <input type="submit" name="submit" value="Load" style="float:left;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" />
            <span class="mdl-textfield__error"><?php echo $error; ?></span>
        </div>
    </div>
    </form>
<?php if ( isset ($average) && $average != null): ?>
<div id="mainProdDiv">
    <h2>Best Three Products</h2>
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" id="tablebestProd" style="margin: 25px auto; width:100%">
        <thead>
            <tr>
                <th class="mdl-data-table__cell--non-numeric">Year</th>
                <th class="mdl-data-table__cell--non-numeric">Month</th>
                <th class="mdl-data-table__cell--non-numeric">Total price</th>
                <th class="mdl-data-table__cell--non-numeric">Item count</th>
                <th class="mdl-data-table__cell--non-numeric">Average price</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($average as $item):?>
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['OrderYear'];?>
                    </td>
                    
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['OrderMonth'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['totalPrice'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['itemCount'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['averageItemPrice'];?>
                    </td>
                </tr>
                <?php endforeach;?>
        </tbody>
    </table>
</div>
<?php endif; ?>