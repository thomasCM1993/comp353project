<script>
$(document).ready(function(){
$("#AccountSetting").on("click", function(){
    console.log("init");
    $( ".Settings" ).slideToggle( "slow");
    
});
    $("#PerviousOrders").on("click", function(){
    console.log("init");
    $( ".Orders" ).slideToggle( "slow");
    
});
})    
</script>
<style>
    .hoverColor:hover{
        background-color: rgba(0, 0, 0, 0.12);
    }
</style>

<div class="demo-card-wide mdl-card mdl-shadow--2dp" id="loginform" style="margin:50px auto;
  width: 100%;">
<?php echo form_open('login/UpdateUser'); ?>
    
        <div class="mdl-card__title hoverColor" id="AccountSetting" style="height: 100px;">
            <h2 class="mdl-card__title-text">Account settings</h2>
        </div>
    <?php if(isset($_SESSION['logged_in'])): ?>
         <div class="mdl-card__supporting-text Settings" style="height: 320px; Display:none">
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" >
        <input class="mdl-textfield__input" type="text" id="fname" name="fname" value="<?php echo $cusData->cus_Firstname; ?>">
        <label class="mdl-textfield__label" for="fname">First Name...</label>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="lname" name="lname" value="<?php echo $cusData->cus_LastName; ?>">
        <label class="mdl-textfield__label" for="lname">Last Name...</label>
    </div>
    
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="emailTextr" name="emailTextr" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="<?php echo $cusData->cus_Email; ?>">
        <label class="mdl-textfield__label" for="emailTextr">Email...</label>
        <span class="mdl-textfield__error">Input is not an email!</span>
    </div>
    
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="password" id="passwordTextr" name="passwordTextr" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" value="<?php echo $cusData->cus_password; ?>">
        <label class="mdl-textfield__label" for="passwordTextr">Password...</label>
        <span class="mdl-textfield__error">must contain 8 or more characters that are of at least one number, and one uppercase and lowercase letter</span>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:80%">
        <input class="mdl-textfield__input" type="text" id="address" name="address" value="<?php echo $cusData->cus_ShippingAddress; ?>">
        <label class="mdl-textfield__label" for="address">Shipping Address (Optional)...</label>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="phone" name="phone" pattern="\d{3}[\-]\d{3}[\-]\d{4}">
        <label class="mdl-textfield__label" for="phone" value="<?php echo $cusData->cus_Phone; ?>">Phone number (Optional)...</label>
        <span class="mdl-textfield__error">Input is not an phone number!</span>
    </div>
    
        </div>
        <div class="mdl-card__actions mdl-card--border Settings" style="Display:none">
            <input type="submit" name="submit" value="Sign up" style="float:right;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/>
            <input type="button" id="Member" name="member" value="Already a member?" style="float:left;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/>
        </div>
    <?php endif; ?>
</form>
    <div class="mdl-card__title hoverColor" id="PerviousOrders" style="height: 100px;">
            <h2 class="mdl-card__title-text">Orders</h2>
    </div>
<?php if(isset($_SESSION['logged_in'])): ?>
    <div class="mdl-card__supporting-text Orders" style="height: 320px; Display:none">
        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" id="TableOrders" style="margin: 25px auto; width:100%">
        <thead>
            <tr>
                <th class="mdl-data-table__cell--non-numeric">Product Name</th>
                <th class="mdl-data-table__cell--non-numeric">Unit Price</th>
                <th>Quantaty Ordered</th>
                <th class="mdl-data-table__cell--non-numeric">Total</th>
                <th class="mdl-data-table__cell--non-numeric">Address Shipped</th>
                <th class="mdl-data-table__cell--non-numeric">Payment Method</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($order_list as $item):?>
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php //echo $item['Department_ID'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php //echo $item['Department_Name'];?>
                    </td>
                    <td>
                        <?php //echo $item['Department_RoomNumber'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php //echo $item['Department_FaxNumber'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php //echo $item['Department_phone1'];?> <br/>
                        <?php //echo $item['Department_phone2'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                    </td>
                </tr>
                <?php endforeach;?>
        </tbody>
    </table>
        
        
        
    </div>
    <div class="mdl-card__actions mdl-card--border Orders" style="Display:none">
        
    </div>
    <?php endif; ?>

</div>