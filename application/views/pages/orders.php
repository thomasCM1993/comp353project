
<div id="mainEmployeeDiv">
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" id="tableDepartment" style="margin: 25px auto; width:100%">
        <thead>
            <tr>
                <th class="mdl-data-table__cell--non-numeric">Order Number</th>
                <th class="mdl-data-table__cell--non-numeric">Order Date</th>
                <th class="mdl-data-table__cell--non-numeric">Order Total</th>
                <th class="mdl-data-table__cell--non-numeric">Invoice</th>
                <th class="mdl-data-table__cell--non-numeric">Edit Shipping</th>
                <th class="mdl-data-table__cell--non-numeric">Delete</th>
                  </tr>
        </thead>
        <tbody>
            <?php foreach ($list_orders as $item):?>
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['OrderNumber'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['OrderDate'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['orderTotal'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" href="<?php echo site_url("Invoice/index/".$item['OrderNumber']); ?>">
                            View Invoice
                        </a>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" href="<?php echo site_url("Shipping/index/".$item['OrderNumber']); ?>">
                        </a>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <a class="mdl-button mdl-js-button mdl-button--fab" id="btnRemoveOrder" href="<?php echo site_url("Order/removeOrder/".$item['OrderNumber']); ?>" >
                        </a>
                    </td>    
                </tr>
                <?php endforeach;?>
        </tbody>
    </table>
</div>
