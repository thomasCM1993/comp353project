<?php echo $emptyCart = True; ?>
<?php echo form_open('Product/update'); ?>

<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" id="tableEmployees" style="margin: 25px auto; width:100%">
    <thead>
            <tr>
                <th>QTY</th>
                <th class="mdl-data-table__cell--non-numeric">Item Description</th>
                <th class="mdl-data-table__cell--non-numeric">Item Price</th>
                <th class="mdl-data-table__cell--non-numeric">Sub-Total</th>
            </tr>
        </thead>
    <tbody>

<?php $i = 1; ?>

<?php foreach ($this->cart->contents() as $items): ?>

	<?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>
        <?php $emptyCart = False;?>
	<tr>
	    <td>
         <?php echo form_input(array('name' => $i.'[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '5', 'min' => '0')); ?></td>
    <td class="mdl-data-table__cell--non-numeric">
		<?php echo $items['name']; ?>
          
			<?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>

				<p>
					<?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>

						<strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />

					<?php endforeach; ?>
				</p>

			<?php endif; ?>

	  </td>
	  <td class="mdl-data-table__cell--non-numeric"><?php echo $this->cart->format_number($items['price']); ?></td>
	  <td class="mdl-data-table__cell--non-numeric">$<?php echo $this->cart->format_number($items['subtotal']); ?></td>
	</tr>

<?php $i++; ?>

<?php endforeach; ?>

<tr>
  <td colspan="2"> </td>
  <td class="mdl-data-table__cell--non-numeric"><strong>Total</strong></td>
  <td class="mdl-data-table__cell--non-numeric">$<?php echo $this->cart->format_number($this->cart->total()); ?></td>
</tr>
</tbody>
</table>
<p><?php echo form_submit('', 'Update your Cart'); ?>
    
</p>
        <?php echo form_close();?>
<?php echo form_open('Product/destroy'); ?>
<p><?php echo form_submit('', 'Empty Cart'); ?></p>
<?php echo form_close();?>

<?php

        if($emptyCart == True)
            echo "Cart is Empty";
        else if(!isset($_SESSION['logged_in']))
                echo 'Sign-in/Register before Checking out<br><br>';
            else
                echo '<a style="float:right;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" href="checkout/view" > Checkout </a>';
        
?>