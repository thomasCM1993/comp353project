<script>
    $(document).ready(function () {
        $("#SearchBar").keyup(function () {
            _this = this;
            // Show only matching TR, hide rest of them
            $.each($("#tableDepartment tbody tr"), function () {
                if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                    $(this).hide();
                else
                    $(this).show();
            });
        });

    });
</script>

<center>
    <div class="mdl-textfield mdl-js-textfield" style="margin-top: 10px;">
        <input class="mdl-textfield__input" type="text" id="SearchBar">
        <label class="mdl-textfield__label" for="SearchBar">Search...</label>
    </div>

</center>
<div id="mainDepartmentDiv">
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" id="tableDepartment" style="margin: 25px auto; width:100%">
        <thead>
            <tr>
                <th class="mdl-data-table__cell--non-numeric">Customer name</th>
                <th class="mdl-data-table__cell--non-numeric">Phone number</th>
                <th class="mdl-data-table__cell--non-numeric">Email</th>
                <th class="mdl-data-table__cell--non-numeric">Last paid amount</th>
                <th class="mdl-data-table__cell--non-numeric">Last paid date</th>
                <th class="mdl-data-table__cell--non-numeric">Order total</th>
                <th class="mdl-data-table__cell--non-numeric">Outstanding payment</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($payments_list as $item):?>
            <?php if ($item['outstanding'] > 0): ?>
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['cus_Firstname'] . " " . $item['cus_LastName'];?>
                    </td>
                    
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['cus_Phone'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['cus_Email'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['amount'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['payDate'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['orderTotal'];?> 
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['outstanding'];?> 
                    </td>
                </tr>
            <?php endif; ?>
            <?php endforeach;?>
        </tbody>
    </table>
</div>