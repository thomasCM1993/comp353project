 <?php foreach ($list_customerInfo as $item):?>
First Name:   <?php echo $item['cus_Firstname'];?><br>
Last Name:    <?php echo $item['cus_LastName'];?><br>
Shipping Address:   <?php echo $item['cus_ShippingAddress'];?><br>
Phone:   <?php echo $item['cus_Phone'];?><br>
Order Number:   <?php echo $item['orderNo'];?><br>
Order Received:   <?php echo $item['receivedDate'];?><br>
Order Shipped:   <?php echo $item['shipDate'];?><br>

<br>
Payment<br>
Payment Type:   <?php echo $item['ccType'];?><br>
Payment Address:   <?php echo $item['ccAddr'];?><br>
<br>
Order Total: <?php echo $item['orderTotal'];?> <br>
<br>
<?php endforeach;?>
<?php $total=0;?>


<table border="1">
<thead>
    <tr>
    <th>Product Name</th>
    <th>Product Color</th>
    <th>Price Per Unit</th>
    <th>Quantity</th>
    <th>Subtotal</th>
    </tr>
    </thead>
    
<?php foreach ($list_orderInfo as $item):?>
    
    <tr>
    <th><?php echo $item['pName'];?></th>
    <th><?php echo $item['Item_Color'];?></th>
    <th><?php echo $item['PriceOfUnit'];?></th>
    <th><?php echo $item['oQuantity'];?></th>
    <th><?php echo $item['PriceOfUnit']*$item['oQuantity']; $total+=$item['PriceOfUnit']*$item['oQuantity']?></th>
    </tr>

<?php endforeach;?>    
    <tr>
    <th></th>
    <th></th>
    <th></th>
    <th>Total</th>
    <th><?php echo $total?></th>
    </tr>   
</table>
<br><br><br>
<table border="1">
<thead>
    <tr>
    <th>Installment No</th>
    <th>Date</th>
    <th>Amount <?php $count =1;?></th>
    </tr>
</thead>
<?php foreach ($list_paymentInfo as $item):?>
    <tr>
    <th><?php echo $count++;?></th>
    <th><?php echo $item['payDate'];?></th>
    <th><?php echo $item['amount'];?></th>
    </tr>    
<?php endforeach;?>        
</table>
<?php if($count==1) echo "Order was paid in full at the end of the transaction.";?>