<?php if(!isset($_SESSION['logged_in'])){
    redirect('login');
}
        if($this->cart->contents()==null){
    redirect('cart');
}
?>

Payment Information:
<?php echo form_open('checkout/order'); ?>
    <table>
        <tr><td>Address 1:</td><td><input type="text" name="paymentAddress1" id="paymentAddress1" maxlength="20"></td></tr>
        <tr><td>Address 2:</td><td><input type="text" name="paymentAddress2"  id="paymentAddress2"maxlength="20"></td></tr>
        <tr><td>City:</td><td><input type="text" name="ccCity" id="ccCity" maxlength="20"></td></tr>
        <tr><td>Province:</td><td><select name="province">
                  <option value="AB">AB</option>
                  <option value="BC">BC</option>
                  <option value="MB">MB</option>
                  <option value="NB">NB</option>
                  <option value="NL">NL</option>
                  <option value="NS">NS</option>
                  <option value="NT">NT</option>
                  <option value="NU">NU</option>
                  <option value="ON">ON</option>
                  <option value="PE">PE</option>
                  <option value="QC">QC</option>
                  <option value="SK">SK</option>
                  <option value="YT">YT</option>
          </select></td></tr>
    <tr><td>Postal Code:</td><td><input type="text" name="ccPostal_Code" id="ccPostal_Code" maxlength="6"></td></tr>
    <tr><td>Country: </td><td><input type="text" name="ccCountry" id="ccCountry"></td></tr>
        
    <tr><td>Type:</td><td><select name="ccType">
                  <option value="Master">Mastercard</option>
                  <option value="Visa">Visa</option> 
        </select></td></tr>
    <tr><td>CreditCard Number</td><td><input type="number" name="ccNum" min="1000000000000000"max="9999999999999999"></td></tr>
    <tr><td>CVS</td><td><input type="number" name="CVS" min="100"max="999"></td></tr>
    <tr><td>Expiry Date</td><td><input type="date" name="expiry" min="<?php echo date("Y-m-d");?>"></td></tr>
    </table>
    <br>
  <input type="radio" name="pType" value="installment" checked>
    <?php 
    $ptotal=$this->cart->total()/4; 
    echo "Installments: ".$ptotal." per Month";
    ?>
    <br>
  <input type="radio" name="pType" value="fullpayment"> <?php echo $this->cart->format_number($this->cart->total());?><br>
    
<input type="submit" name="submit" value="Create Order" style="float:right;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/>
 <?php echo form_close();?>