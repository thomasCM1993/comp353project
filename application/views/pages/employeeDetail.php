<script>
    $(document).ready(function(){
        
        $("#PartTime-1").focus(function(){
            //$("#Salary").val("");
            $("#fullTimeDiv").slideUp();
            $("#partTimeDiv").slideDown();
            
        });
        $("#FullTime-2").focus(function(){
            //$("#HourlyRate").val("");
            $("#NumberOfHoursPerWeek").val("");
            $("#partTimeDiv").slideUp();
            $("#fullTimeDiv").slideDown();
        });
        
    });
</script>


<?php echo form_open('employeeDetail/UpdateEmployee/' . $employee->employee_ID); ?>
    <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="AddEmployeeDiv" style="margin:50px auto;
  height: 300px;
  width: 80%;">
        <div class="mdl-card__title" style="height: 100px;">
            <h2 class="mdl-card__title-text">Employee Detail</h2>
        </div>
        <div class="mdl-card__supporting-text" style="height: 420px">

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="e_firstName" id="e_firstName" value="<?php echo $employee->e_firstName; ?>">
                <label class="mdl-textfield__label" for="e_firstName">First name</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="e_lastName" id="e_lastName" value="<?php echo $employee->e_lastName; ?>">
                <label class="mdl-textfield__label" for="e_lastName">Last name</label>
            </div>

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="date" name="e_dateOfBirth" id="e_dateOfBirth" value="<?php echo $employee->e_dateOfBirth; ?>">
                <label class="mdl-textfield__label" for="e_dateOfBirth">Date of birth</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="e_position" id="e_position" value="<?php echo $employee->e_position; ?>">
                <label class="mdl-textfield__label" for="e_position">position</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="e_address" id="e_address" value="<?php echo $employee->e_address; ?>">
                <label class="mdl-textfield__label" for="e_address">Address</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="number" name="e_SIN" id="e_SIN" disabled value="<?php echo $employee->e_SIN; ?>">
                <label class="mdl-textfield__label" for="e_SIN">SIN</label>
            </div>
            <br/>

        </div>
        <div class="mdl-card__actions mdl-card--border">
            <input type="submit" name="submit" value="Update Info" style="float:left;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" />
            <span class="mdl-textfield__error"><?php echo $error; ?></span>
        </div>
    </div>
    </form>


<?php echo form_open('employeeDetail/UpdateDependant/' . $employee->employee_ID); ?>
    <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="AddEmployeeDiv" style="margin:50px auto;
  height: 300px;
  width: 80%;">
        <div class="mdl-card__title" style="height: 100px;">
            <h2 class="mdl-card__title-text">Dependant Detail</h2>
        </div>
        <div class="mdl-card__supporting-text" style="height: 420px">

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="D_firstName" id="D_firstName" value="<?php echo $employeeDependent->D_firstName; ?>">
                <label class="mdl-textfield__label" for="D_firstName">Dependant's first name</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="D_lastName" id="D_lastName" value="<?php echo $employeeDependent->D_lastName; ?>">
                <label class="mdl-textfield__label" for="D_firstName">Dependant's last name</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="date" name="D_dateOfBirth" id="D_dateOfBirth" value="<?php echo $employeeDependent->D_dateOfBirth; ?>">
                <label class="mdl-textfield__label" for="D_dateOfBirth">Dependant's date of birth</label>
            </div>

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="D_phone1" id="D_phone1" value="<?php echo $employeeDependent->D_phone1; ?>">
                <label class="mdl-textfield__label" for="D_phone">Dependant's Primary Phone Number</label>
            </div>
            
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="D_phone2" id="D_phone2" value="<?php echo $employeeDependent->D_phone2; ?>">
                <label class="mdl-textfield__label" for="D_phone">Dependant's Secondary Phone Number</label>
            </div>
            
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="Dependant_SIN" id="Dependant_SIN" value="<?php echo $employeeDependent->Dependant_SIN; ?>">
                <label class="mdl-textfield__label" for="Dependant_SIN">Dependant's SIN</label>
            </div>
            <br/>

        </div>
        <div class="mdl-card__actions mdl-card--border">
            <input type="submit" name="submit" value="Update Info" style="float:left;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" />
            <span class="mdl-textfield__error"><?php echo $error; ?></span>
        </div>
    </div>
    </form>

<?php echo form_open('employeeDetail/EmployeeTime/' . $employee->employee_ID); ?>
    <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="AddEmployeeDiv" style="margin:50px auto;
  height: 300px;
  width: 80%;">
        <div class="mdl-card__title" style="height: 100px;">
            <h2 class="mdl-card__title-text">Employee Availability</h2>
        </div>
        <div class="mdl-card__supporting-text" style="height: 420px">

            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="PartTime-1">
                <input type="radio" id="PartTime-1" class="mdl-radio__button" name="options" value="1" checked>
                <span class="mdl-radio__label">Part Time</span>
            </label>
            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="FullTime-2">
                <input type="radio" id="FullTime-2" class="mdl-radio__button" name="options" value="2">
                <span class="mdl-radio__label">Full Time</span>
            </label>
            <div id="partTimeDiv">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input class="mdl-textfield__input" type="text" name="HourlyRate" id="HourlyRate" value="<?php if($employeePartTime != "" && $employeePartTime->HourlyRate != "") echo $employeePartTime->HourlyRate; ?>">
                    <label class="mdl-textfield__label" for="HourlyRate">Hourly Rate</label>
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input class="mdl-textfield__input" type="text" name="NumberOfHoursPerWeek" id="NumberOfHoursPerWeek" value="<?php if($employeePartTime != "" && $employeePartTime->NumberOfHoursPerWeek != "") echo $employeePartTime->NumberOfHoursPerWeek; ?>">
                    <label class="mdl-textfield__label" for="NumberOfHoursPerWeek">Number Of Hours Per Week</label>
                </div>

            </div>
            <div id="fullTimeDiv" style="display:none">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input class="mdl-textfield__input" type="text" name="Salary" id="Salary" value="<?php if($employeeFullTime != "" && $employeeFullTime->Salary != "") echo $employeeFullTime->Salary; ?>">
                    <label class="mdl-textfield__label" for="Salary">Salary</label>
                </div>

            </div>
            <br/>

        </div>
        <div class="mdl-card__actions mdl-card--border">
            <input type="submit" name="submit" value="Update Info" style="float:left;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" />
            <span class="mdl-textfield__error"><?php echo $error; ?></span>
        </div>
    </div>
    </form>