<?php echo form_open('Shipping/updateShipping/'.$shippingInfo->shipmentNo); ?>

        <div class="mdl-card__title" style="height: 100px;">
            <h2 class="mdl-card__title-text">Shipping Detail</h2>
        </div>
        <div class="mdl-card__supporting-text" style="height: 420px">

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="date" name="receivedDate" id="e_firstName" min="<?php echo $shippingInfo->receivedDate; ?>" max="<?php echo $shippingInfo->shipDate; ?>" value="<?php echo $shippingInfo->receivedDate; ?>">
                <label class="mdl-textfield__label" for="dateBought">Ordered:</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="date" name="shipDate" id="e_lastName" min="<?php echo $shippingInfo->receivedDate; ?>" value="<?php echo $shippingInfo->shipDate; ?>">
                <label class="mdl-textfield__label" for="e_lastName">Shipped:</label>
            </div>
            
        </div>
        <div class="mdl-card__actions mdl-card--border">
            <input type="submit" name="submit" value="Update Info" style="float:left;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" />
        </div>

    <?php echo form_close();?>