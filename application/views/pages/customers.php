<div id="mainEmployeeDiv">
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" id="tableEmployees" style="margin: 25px auto; width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th class="mdl-data-table__cell--non-numeric">Name</th>
                <th class="mdl-data-table__cell--non-numeric">Address</th>
                <th class="mdl-data-table__cell--non-numeric">Phone</th>
                <th class="mdl-data-table__cell--non-numeric">Email</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list_customers as $item):?>
                <tr>
                    <td>
                        <?php echo $item['cus_ID'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['cus_Firstname'] . " " . $item['cus_LastName'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['cus_ShippingAddress'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['cus_Phone'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['cus_Email'];?>
                    </td>
                                        <td class="mdl-data-table__cell--non-numeric">
                      <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" href="<?php echo site_url("Order/index/".$item['cus_ID']); ?>" >
                        Order(s)
                                            </a>
                    </td>                        
                    
                    <td class="mdl-data-table__cell--non-numeric">
                        <a class="mdl-button mdl-js-button mdl-button--fab" id="btnRemoveEmployee" href="<?php echo site_url("customers/remove_customer/".$item['cus_ID']); ?>" >
                        <i class="material-icons">remove</i>
                        </a>
                        
                    </td>
                </tr>
                <?php endforeach;?>
        </tbody>
    </table>
</div>