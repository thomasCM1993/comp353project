Full Time Employees
<div id="mainEmployeeDiv">
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" id="tableDepartment" style="margin: 25px auto; width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th class="mdl-data-table__cell--non-numeric">Name</th>
                <th class="mdl-data-table__cell--non-numeric">Position</th>
                <th class="mdl-data-table__cell--non-numeric">Start date</th>
                <th class="mdl-data-table__cell--non-numeric">End date</th>
                <th class="mdl-data-table__cell--non-numeric">Salary</th>
                <th class="mdl-data-table__cell--non-numeric">Number of Dependants</th>
                <th class="mdl-data-table__cell--non-numeric">View Detail</th>
                
            </tr>
        </thead>
        <tbody>
            <?php foreach ($DepHisFT as $item):?>
                <tr>
                    <td>
                        <?php echo $item['Employee_ID'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['e_firstName'] . " " . $item['e_lastName'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['e_position'] ;?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['startDate'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php if($item['EndDate'] != null && $item['EndDate'] !="" ) {echo $item['EndDate'];}else{ echo "No data";}?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['Salary'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['numberofDep'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" href="<?php echo site_url("employeeDetail/index/".$item['Employee_ID']); ?>">
                            View Detail
                        </a>
                        
                    </td>
                </tr>
                <?php endforeach;?>
        </tbody>
    </table>
</div>
Part Time Employees
<div id="mainEmployeeDiv">
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" id="tableDepartment" style="margin: 25px auto; width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th class="mdl-data-table__cell--non-numeric">Name</th>
                <th class="mdl-data-table__cell--non-numeric">Position</th>
                <th class="mdl-data-table__cell--non-numeric">Start date</th>
                <th class="mdl-data-table__cell--non-numeric">End date</th>
                <th class="mdl-data-table__cell--non-numeric">Monthy Salary</th>
                <th class="mdl-data-table__cell--non-numeric">Number of Dependants</th>
                <th class="mdl-data-table__cell--non-numeric">View Detail</th>
                  </tr>
        </thead>
        <tbody>
            <?php foreach ($DepHisPT as $item):?>
                <tr>
                    <td>
                        <?php echo $item['Employee_ID'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['e_firstName'] . " " . $item['e_lastName'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['e_position'] ;?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['startDate'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php if($item['EndDate'] != null && $item['EndDate'] !="" ) {echo $item['EndDate'];}else{ echo "No data";}?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['HourlyRate'] * $item['NumberOfHoursPerWeek'] * 4;?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <?php echo $item['numberofDep'];?>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" href="<?php echo site_url("employeeDetail/index/".$item['Employee_ID']); ?>">
                            View Detail
                        </a>
                        
                    </td>
                </tr>
                <?php endforeach;?>
        </tbody>
    </table>
</div>

<?php echo form_open('DepartmentHistory/addEmployeeToDepartment/'. $department); ?>
    <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="AddDepartmentForm" style="width:60%; margin:50px auto;">
        <div class="mdl-card__title">
            <h2 class="mdl-card__title-text">Add Employee to department</h2>
        </div>
        <div class="mdl-card__supporting-text" style="height: 60px">
            <span>Employees:</span>
            <select id="Employees" name="Employees">
                <?php foreach ($Employee_list as $item):?>
                <option value="<?php echo $item['employee_ID'];?>"> <?php echo $item['e_firstName'] . " " . $item['e_lastName'];?></option >
                <?php endforeach;?>
            </select>
        </div>
        <div class="mdl-card__actions mdl-card--border">
            <input type="submit" name="submit" value="Add" style="float:right;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/>
        </div>
    </div>
</form>