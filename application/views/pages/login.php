



<!-- Square card -->
<style>
.demo-card-wide > .mdl-card__menu {
  color: #fff;
}
</style>
<script>
    $(document).ready(function(){
        $("#Registerform").hide();
        $("#NotAMember").click(function () {
            $("#loginform").css("position","absolute");
            $("#Registerform").css("position","static");
            $("#Registerform").show("slide", { direction: "right" }, 500);
            $("#loginform").hide("slide", { direction: "left" }, 500);
            
        });
        
        $("#Member").click(function () {
            $("#Registerform").css("position","absolute");
            $("#Registerform").hide("slide", { direction: "right" }, 500);
            $("#loginform").css("position","static");
            $("#loginform").show("slide", { direction: "left" }, 500);
            
        });
    });
    
</script>

<?php echo validation_errors(); ?>

<?php echo form_open('login/loginuser'); ?>
    <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="loginform" style="margin:50px auto;
  height: 500px;
  width: 40%;">
        <div class="mdl-card__title" style="height: 100px;">
            <h2 class="mdl-card__title-text">Login</h2>
        </div>
        <div class="mdl-card__supporting-text" style="height: 420px">
            
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" name="emailText" id="emailText" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
        <label class="mdl-textfield__label" for="emailText">Email...</label>
        <span class="mdl-textfield__error">Input is not an email!</span>
    </div>
    
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="password" name="passwordText" id="passwordText" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">
        <label class="mdl-textfield__label" for="passwordText">Password...</label>
        <span class="mdl-textfield__error">must contain 8 or more characters that are of at least one number, and one uppercase and lowercase letter</span>
    </div>
        </div>
        <div class="mdl-card__actions mdl-card--border">
            <input type="submit" name="submit" value="Sign in" style="float:left;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/>
            <span class="mdl-textfield__error"><?php echo $error; ?></span>
            <input type="button" id="NotAMember" name="notmember" value="Not a member?" style="float:right;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/>
        </div>
    </div>
</form>

<?php echo form_open('login/createuser'); ?>
    <div class="demo-card-wide mdl-card mdl-shadow--2dp" id="Registerform" style="width:60%; margin:50px auto;">
        <div class="mdl-card__title">
            <h2 class="mdl-card__title-text">Register</h2>
        </div>
        <div class="mdl-card__supporting-text" style="height: 420px">
            
            
    
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="fname" name="fname">
        <label class="mdl-textfield__label" for="fname">First Name...</label>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="lname" name="lname">
        <label class="mdl-textfield__label" for="lname">Last Name...</label>
    </div>
    
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="emailTextr" name="emailTextr" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
        <label class="mdl-textfield__label" for="emailTextr">Email...</label>
        <span class="mdl-textfield__error">Input is not an email!</span>
    </div>
    
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="password" id="passwordTextr" name="passwordTextr" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">
        <label class="mdl-textfield__label" for="passwordTextr">Password...</label>
        <span class="mdl-textfield__error">must contain 8 or more characters that are of at least one number, and one uppercase and lowercase letter</span>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:80%">
        <input class="mdl-textfield__input" type="text" id="address" name="address">
        <label class="mdl-textfield__label" for="address">Shipping Address (Optional)...</label>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" id="phone" name="phone" pattern="\d{3}[\-]\d{3}[\-]\d{4}">
        <label class="mdl-textfield__label" for="phone">Phone number (Optional)...</label>
        <span class="mdl-textfield__error">Input is not an phone number!</span>
    </div>
    
        </div>
        <div class="mdl-card__actions mdl-card--border">
            <input type="submit" name="submit" value="Sign up" style="float:right;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/>
            <input type="button" id="Member" name="member" value="Already a member?" style="float:left;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/>
        </div>
    </div>
</form>