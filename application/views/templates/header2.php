<html>

<head>
    <title>COMP 353</title>
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.amber-green.min.css" />
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
</head>

<body>
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <header class="mdl-layout__header">
            <div class="mdl-layout__header-row">
                <!-- Title -->
                <a class="mdl-navigation__link" href="<?php echo site_url("home")?>"><span class="mdl-layout-title">bmc353_4</span></a>
                <!-- Add spacer, to align navigation to the right -->
                <div class="mdl-layout-spacer"></div>
                <!-- Navigation. We hide it in small screens. -->
                <nav class="mdl-navigation mdl-layout--large-screen-only">
                    <a class="mdl-navigation__link" href="<?php echo site_url("employee/view")?>">Employee</a>
                    <a class="mdl-navigation__link" href="<?php echo site_url("inventory/view")?>">Products</a>
                    <a class="mdl-navigation__link" href="<?php echo site_url("department/view")?>">Departments</a>
                    <a class="mdl-navigation__link" href="<?php echo site_url("customers/view")?>">Customers</a>
                    <a class="mdl-navigation__link" href="<?php echo site_url("OutstandingPayments/view")?>">Outstanding Payments</a>
                    <a class="mdl-navigation__link" href="<?php echo site_url("ThreeBest/view")?>">Three Best</a>
                </nav>
            </div>
        </header>
        <div class="mdl-layout__drawer">
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link" href="<?php echo site_url("employee/view ")?>">Employee</a>
                <a class="mdl-navigation__link" href="<?php echo site_url("inventory/view ")?>">Products</a>
                <a class="mdl-navigation__link" href="<?php echo site_url("department/view ")?>">Departments</a>
                <a class="mdl-navigation__link" href="<?php echo site_url("customers/view")?>">Customers</a>
                <a class="mdl-navigation__link" href="<?php echo site_url("OutstandingPayments/view")?>">Outstanding Payments</a>
                <a class="mdl-navigation__link" href="<?php echo site_url("ThreeBest/view")?>">Three Best</a>
                <a class="mdl-navigation__link" href="<?php echo site_url("home")?>">Customer Website</a>
                
            </nav>
        </div>
        <main class="mdl-layout__content">
            <div class="page-content" style="width: 80%; margin:0 auto;">
                            <!-- Your content goes here -->
                            <div class="demo-card-wide mdl-card mdl-shadow--2dp" style="width:100%; padding:1%; margin-top:50px;min-height: 100px;">
                                <div class="mdl-card__title">
                                    <h2 class="mdl-card__title-text"><?php echo $title; ?></h2>
                                </div>
                            </div>