<html>

<head>
    <title>COMP 353</title>
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.amber-green.min.css" />
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.6/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
</head>

<body>

    <body>

        <style>
            .demo-layout-waterfall .mdl-layout__header-row .mdl-navigation__link:last-of-type {
                padding-right: 0;
            }
        </style>

        <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
            <header class="mdl-layout__header">
                <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
                    <header class="mdl-layout__header">
                        <div class="mdl-layout__header-row">
                            <!-- Title -->
                            <a class="mdl-navigation__link" href="<?php echo site_url("home")?>"><span class="mdl-layout-title">bmc353_4</span></a>
                            <div class="mdl-layout-spacer"></div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable
                  mdl-textfield--floating-label mdl-textfield--align-right">
                                <label class="mdl-button mdl-js-button mdl-button--icon" for="waterfall-exp">
                                    <i class="material-icons">search</i>
                                </label>
                                <div class="mdl-textfield__expandable-holder">
                                    <input class="mdl-textfield__input" type="text" name="sample" id="waterfall-exp">
                                </div>
                            </div>
                        </div>
                        <!-- Bottom row, not visible on scroll -->
                        <div class="mdl-layout__header-row">
                            <div class="mdl-layout-spacer"></div>
                            <!-- Navigation -->
                            <nav class="mdl-navigation">
                                <?php if(isset($_SESSION['logged_in'])): ?>
                                
                                    <a id="logout" class="mdl-navigation__link" href="<?php echo site_url("login/logout")?>">
                                            <?php echo $_SESSION['username']; ?>
                                    </a>
                                    <div class="mdl-tooltip" for="logout">
                                            logout
                                    </div>
                                <?php else: ?>
                                    <a class="mdl-navigation__link" href="<?php echo site_url("login/view")?>">Login</a>
                                <?php endif; ?>
                                
                                <a class="mdl-navigation__link" href="<?php echo site_url("account/view")?>">Account</a>
                                <a class="mdl-navigation__link" href="<?php echo site_url("cart")?>">
                                    <?php if($this->cart->total_items() > 0): ?>
                                    <div class="material-icons mdl-badge mdl-badge--overlap" data-badge="<?php echo $this->cart->total_items();?>">shopping_cart</div>
                                    <?php else: ?>
                                    <i class="material-icons">shopping_cart</i>
                                    <?php endif; ?>
                                </a>
                            </nav>
                        </div>
                    </header>
                    <div class="mdl-layout__drawer">
                        <nav class="mdl-navigation">
                            <a class="mdl-navigation__link" href="<?php echo site_url("employee/view ")?>">Employee Website</a>
                        </nav>
                    </div>
                    <main class="mdl-layout__content">
                        <div class="page-content" style="width: 80%; margin:0 auto;">
                            <!-- Your content goes here -->
                            <div class="demo-card-wide mdl-card mdl-shadow--2dp" style="width:100%; padding:1%; margin-top:50px;min-height: 100px;">
                                <div class="mdl-card__title">
                                    <h2 class="mdl-card__title-text"><?php echo $title; ?></h2>
                                </div>
                            </div>