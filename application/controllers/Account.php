<?php
class Account extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Customer_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('cart');
    }
    public function view($page = "account")
    {
        
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }
        
        $data['title'] = ucfirst($page); 
        
        $id = isset($_SESSION['userID']) ? $this->session->userdata('userID') : 0;
        $data['cusData'] = $this->Customer_model->Get_Indiv_CustomerInfo($id);
        //$date['order_list'] = array("id"=> "ss");
        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
    
}