<?php
class DepartmentHistory extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Department_model');
        $this->load->model('employment_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('cart');
    }
    
    public function index($department)
    {
        $data['department'] = $department;
        $data['DepHisFT'] = $this->Department_model->get_department_historyFT($department);
        $data['DepHisPT'] = $this->Department_model->get_department_historyPT($department);
        $data['Employee_list'] = $this->employment_model->get_Employees();
        $data['title'] = "Department History";
        $this->load->view('templates/header2', $data);
        $this->load->view('pages/departmenthistory', $data);
        $this->load->view('templates/footer', $data);
    }
    public function view($page = "departmenthistory")
    {
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }
        $data['title'] = ucfirst($page);    
        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
    public function addEmployeeToDepartment($id)
    {
        $this->Department_model->Add_toDepartment($id);
        
        $this->index($id);
    }
}