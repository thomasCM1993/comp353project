<?php
class Invoice extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Invoice_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('cart');
    }
    
    public function index($id)
    {
        $data['list_customerInfo'] = $this->Invoice_model->GetListCustomerInfo($id);
        $data['list_orderInfo'] = $this->Invoice_model->GetListOrderInfo($id);
        $data['list_paymentInfo'] = $this->Invoice_model->GetListInstallments($id);
        $data['title'] = "Invoice";
        $this->load->view('templates/header2', $data);
        $this->load->view('pages/invoice', $data);
        $this->load->view('templates/footer', $data);
    }
    public function view($page = "invoice")
    {
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }
        $data['title'] = ucfirst($page);    
        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
}