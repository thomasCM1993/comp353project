<?php
class Pages extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('product_model');
        $this->load->helper('url_helper');
        $this->load->library('cart');
    }
    public function index()
    {
        $this->load->view('pages/home', $data);
    }

    public function view($page = "home")
    {
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter
            
        $data['product_list'] = $this->product_model->get_products();
        
        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
}