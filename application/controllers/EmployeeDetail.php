<?php
class EmployeeDetail extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('employment_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('cart');
    }
    
    public function index($employeeID)
    {
        $data['employee'] = $this->employment_model->get_specific_employee($employeeID);
        $data['employeeDependent'] = $this->employment_model->get_specific_employeeDependent($employeeID);
        $data['employeePartTime'] = $this->employment_model->get_specific_employeePartTime($employeeID);
        $data['employeeFullTime'] = $this->employment_model->get_specific_employeeFullTime($employeeID);
        
        $data['title'] = "Employee Detail";
        $this->load->view('templates/header2', $data);
        $this->load->view('pages/employeeDetail', $data);
        $this->load->view('templates/footer', $data);
    }
    
    public function view($page = "employeeDetail")
    {
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }
        $data['title'] = ucfirst($page);    
        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
    
    public function UpdateEmployee($employeeID)
    {
        $this->employment_model->update_specific_employee($employeeID);
        $this->index($employeeID);
    }
    public function UpdateDependant($employeeID)
    {
        $this->employment_model->update_specific_employeeDependent($employeeID);
        $this->index($employeeID);
    }
    public function EmployeeTime($employeeID)
    {
        $this->employment_model->update_specific_employeeAvailability($employeeID);
        $this->index($employeeID);
    }
    
}