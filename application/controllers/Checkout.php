<?php
class Checkout extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Checkout_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('cart');
    }
    
    public function view($page = "checkout")
    {
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }
        $data['title'] = ucfirst($page);  
        $id = isset($_SESSION['userID']) ? $this->session->userdata('userID') : 0;
        $data['cus_Data'] = $this->Checkout_model->Get_CustomerInfo($id);
        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
    
    public function order(){
        
        //Create Order
        $this->Checkout_model->createOrder();
        $cusID=$_SESSION['userID'];
        $cusData= $this->Checkout_model->getOrderNo($cusID);
        
        //Create Payment
        $this->Checkout_model->createPayment($cusData->orderNo);
        
        //Create OrderDetails for each item in Cart
         foreach ($this->cart->contents() as $items){
            $oQuantity = $items['qty'];
            $itemNo = $items['id'];
            $this->Checkout_model->createDetails($cusData->orderNo, $oQuantity,$itemNo);
            $this->Checkout_model->updateInventory($itemNo, $oQuantity);
              
        
        }
        
        //Creates Payment Installments or Full Payment records
        $cusPay= $this->Checkout_model->getPaymentNo($cusData->orderNo);
        if($this->input->post('pType')=='installment')
            $this->Checkout_model->createInstallment($cusPay->paymentNo);
        else
             $this->Checkout_model->createFullPayment($cusPay->paymentNo);
        
         $this->Checkout_model->createShippingInfo($cusData->orderNo);
        
        $this->cart->destroy();
        
        echo "Order Created!";
    }
   
   
}