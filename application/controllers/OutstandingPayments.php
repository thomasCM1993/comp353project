<?php
class OutstandingPayments extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Order_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('cart');
    }
    
    public function view($page = "outstandingpayments")
    {
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }
        $data['title'] = "Outstanding Payments";  
        $data['payments_list'] = $this->Order_model->get_outstanding_payments();
        $this->load->view('templates/header2', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
    
    
}