<?php
class Department extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Department_model');
        $this->load->model('employment_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('cart');
    }
    
    public function view($page = "department")
    {
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }
        $data['title'] = ucfirst($page);  
        $data['department_list'] = $this->Department_model->get_departments();
        $data['Employee_list'] = $this->employment_model->get_Employees();
        $this->load->view('templates/header2', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
    public function createDepartment()
    {
        $this->form_validation->set_rules('Department_Name', 'Department_Name', 'required');
        $this->form_validation->set_rules('Department_RoomNumber', 'Department_RoomNumber', 'required');
        $this->form_validation->set_rules('Department_phone1', 'Department_phone1', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->view();

        }
        else
        {
            $this->Department_model->create_new_department();
        }
        $this->view();
    }
    public function remove_Department($id)
    {
        $this->Department_model->Delete_Department($id);
        $this->view();
    }
}