<?php
class Customers extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Customer_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('cart');
    }
    
    public function view($page = "customers")
    {
        
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }
        $data['list_customers'] = $this->Customer_model->GetListCustomers();
        $data['title'] = ucfirst($page); 
        $this->load->view('templates/header2', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
    public function remove_customer($id)
    {
        $this->Customer_model->DeleteCustomer($id);
        $this->view();
    }
}