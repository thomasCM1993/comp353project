<?php
class Inventory extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('product_model');
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->load->library('cart');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
    
    public function view($page = "inventory")
    {
        
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }
        $data['list_inventory'] = $this->product_model->get_inventory();
        $data['title'] = ucfirst($page); 
        $this->load->view('templates/header2', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
    
    public function createProduct()
    {
        $this->form_validation->set_rules('pName', 'pName', 'required');
        $this->form_validation->set_rules('pDescription', 'pDescription', 'required');
        $this->form_validation->set_rules('PriceOfUnit', 'PriceOfUnit', 'required');
        $this->form_validation->set_rules('NumberOfUnitsProduced', 'NumberOfUnitsProduced', 'required');
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->view();

        }
        else
        {
            $this->product_model->create_new_product();
        }
        $this->view();

    }
    public function incress_inventory($id)
    {
        $this->product_model->incress_inventory($id);
        $this->view();
    }
    public function remove_product($id, $id2)
    {
        $this->product_model->removeProduct($id, $id2);
        $this->view();
    }
    
}