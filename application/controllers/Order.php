<?php
class Order extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Order_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('cart');
    }
    
    public function index($id)
    {
        $data['list_orders'] = $this->Order_model->GetListOrders($id);
        $data['title'] = "Orders";
        $this->load->view('templates/header2', $data);
        $this->load->view('pages/orders', $data);
        $this->load->view('templates/footer', $data);
    }
    public function view($page = "orders")
    {
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }
        $data['title'] = ucfirst($page);    
        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }

    public function removeOrder($orderNo){
        
        $this->Order_model->removeOrder($orderNo);
    }
}