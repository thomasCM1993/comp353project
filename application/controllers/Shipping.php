<?php
class Shipping extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url_helper');
        $this->load->model('Shipping_model');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('cart');
    }
    
    public function index($orderNo)
    {
        $data['title'] = "Shipping";
        $data['shippingInfo'] = $this->Shipping_model->getShippingInfo($orderNo);
        $this->load->view('templates/header2', $data);
        $this->load->view('pages/shipping', $data);
        $this->load->view('templates/footer', $data);
    }
    
    public function view($page = "shipping")
    {
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }
        $data['title'] = ucfirst($page);    
        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
    
    public function updateShipping($shipmentNo){
     $this->Shipping_model->update_shipping($shipmentNo);
    }
}