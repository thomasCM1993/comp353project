<?php
class ThreeBest extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Customer_model');
        $this->load->model('product_model');
        $this->load->model('Order_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('cart');
    }
    
    public function view($page = "best")
    {
        
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }
        $data['title'] = "The Three Best customers and Products"; 
        $data['cus_list'] = $this->Customer_model->best_three_customers();
        $data['prod_list'] = $this->product_model->top_three_selling_products();
        
        $this->load->view('templates/header2', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
    
    public function loadAverage()
    {
        $data["average"] = $this->Order_model->get_average_price();
        $data['title'] = "The Three Best customers and Products"; 
        $data['cus_list'] = $this->Customer_model->best_three_customers();
        $data['prod_list'] = $this->product_model->top_three_selling_products();
        
        $this->load->view('templates/header2', $data);
        $this->load->view('pages/'."best", $data);
        $this->load->view('templates/footer', $data);
        
    }
}