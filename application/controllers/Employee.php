<?php
class Employee extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('employment_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('cart');
    }
    public function index()
    {
        $this->load->view('pages/employee', $data);
    }

    public function view($page = "employee")
    {
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }
        $data['title'] = ucfirst($page);    
        $data['employee_list'] = $this->employment_model->get_Employees();
        $this->load->view('templates/header2', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
    
    public function addEmployee()
    {
        $this->form_validation->set_rules('e_firstName', 'e_firstName', 'required');
        $this->form_validation->set_rules('e_lastName', 'e_lastName', 'required');
        $this->form_validation->set_rules('e_position', 'e_position', 'required');
        $this->form_validation->set_rules('e_SIN', 'e_SIN', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->view();

        }
        else
        {
            $this->employment_model->add_Employees();
        }
        $this->view();
    }
    public function remove_employee($id)
    {
        $this->employment_model->remove_employee($id);
        $this->view();
    }
}