<?php
class Product extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('product_model');
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->load->library('cart');
    }
    public function index($productID)
    {
        $data['product_list'] = $this->product_model->get_specific_product($productID);
        $data['title'] = "Product";
        $this->load->view('templates/header', $data);
        $this->load->view('pages/product', $data);
        $this->load->view('templates/footer', $data);
        
    }

    public function view($page = "product")
    {
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter
        
        
        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
    
    function add(){
        
        
            $insert = array(
            'id' => $this->input->post('id'),
            'name' => $this->input->post('name'),
            'qty' => 1,
            'price' => $this->input->post('price'),
        );
        
        $this->cart->insert($insert);
        redirect('cart');
    }
    
    function show(){
        
        $cart = $this->cart->contents();
        echo "<pre>";
        print_r($cart);
    }
    
    function update(){
    
		$contentsofCart = $this->input->post();
		
		foreach ($contentsofCart as $content)
		{
			$info = array(
		   'rowid' => $content['rowid'],
		   'qty'   => $content['qty']
			 );

			$this->cart->update($info);

		}
        
        redirect('cart');
	}
    
    function destroy(){

        $this->cart->destroy();
        
redirect('cart');

    }
}