<?php
class Login extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Customer_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('cart');
    }
    
    
    public function view($page = "login")
    {
        
        if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
            show_404();
        }
        $data['title'] = ucfirst($page); 
        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
    
    public function loginuser()
    {
        $this->form_validation->set_rules('emailText', 'EmailText', 'required');
        $this->form_validation->set_rules('passwordText', 'PasswordText', 'required');

    if ($this->form_validation->run() === FALSE)
    {
        $this->view();

    }
    else
    {
       $array = $this->Customer_model->Log_in();
        
        if( count($array) === 1)
        {
            $newdata;
            foreach($array as $row){    
            $newdata = array(
                'userID' => $row->cus_ID,
                'username'  => $row->cus_Firstname. " " . $row->cus_LastName,
                'email'     => $row->cus_Email,
                'logged_in' => TRUE
            );
            }

            $this->session->set_userdata($newdata);
            
        }
        $this->view();
    }
    }
    public function logout()
    {
        unset(
        $_SESSION['username'],
        $_SESSION['email'],
        $_SESSION['logged_in']
        );
        $this->view();
    }
    public function createuser()
    {
        $this->form_validation->set_rules('emailTextr', 'EmailTextr', 'required');
        $this->form_validation->set_rules('passwordTextr', 'PasswordTextr', 'required');
        $this->form_validation->set_rules('fname', 'Fname', 'required');
        $this->form_validation->set_rules('lname', 'Lname', 'required');

    if ($this->form_validation->run() === FALSE)
    {
        $this->load->view('templates/header');
        $this->load->view('pages/login');
        $this->load->view('templates/footer');

    }
    else
    {
        
        if($this->Customer_model->Register())
        {
            
        }
        else{
            $this->load->view('templates/header');
            $this->load->view('pages/home');
            $this->load->view('templates/footer');
        }
    }
    }
    
}